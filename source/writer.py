from source import Matrix


class Writer:
    def __init__(self, separator_token: str = ";", output_name: str = "simiana-output", do_ordering: str = 'y'):
        self._output_name = output_name
        self._separator_token = separator_token
        self._do_ordering_of_sequence = self._parse_yes_no(do_ordering)

    def output(self, matrix: Matrix):
        pass

    def _parse_yes_no(self, do_ordering: str) -> bool:
        return 'y' == do_ordering


class PrintWriter(Writer):
    def output(self, matrix: Matrix):
        for name in sorted(matrix.sequence_row_names):
            self._output_sequence_matches(name, matrix)

    def _output_sequence_matches(self, name, matrix):
        sequence = matrix.find_sequence(name)
        print("{}{}{}".format('---', sequence.name, '---'))
        values = sequence.ordered_values if self._do_ordering_of_sequence else sequence.values
        for compared_sequence_name, similarity_value in values:
            if similarity_value is not None:
                print("{}{}{}{}{}".format(
                    sequence.name,
                    self._separator_token,
                    compared_sequence_name,
                    self._separator_token,
                    similarity_value
                ))


class FileWriter(Writer):
    def output(self, matrix: Matrix):
        try:
            with open(self._output_name, 'w') as file:
                for name in sorted(matrix.sequence_row_names):
                    self._output_sequence_matches(name, matrix, file)
        except PermissionError:
            raise IOError('Unable to write to file : {} (PermissionError), is the file in use?'.format(self._output_name))

    def _output_sequence_matches(self, name, matrix: Matrix, file):
        sequence = matrix.find_sequence(name)
        values = sequence.ordered_values if self._do_ordering_of_sequence else sequence.values
        for compared_sequence_name, similarity_value in values:
            if similarity_value is not None:
                line = "{}{}{}{}{}\n".format(
                    sequence.name,
                    self._separator_token,
                    compared_sequence_name,
                    self._separator_token,
                    similarity_value
                )
                file.write(line)
