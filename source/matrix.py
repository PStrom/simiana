from typing import List

from source import GenomeSequence


class Matrix:
    def __init__(self, rows: List[list]):
        self._rows = rows
        self._row_names = [l[0] for l in rows]

        self._named_sequences = dict()

        for row in self._rows:
            self._named_sequences[row[0]] = GenomeSequence(row[0], self._row_names, row[1:])

        self._nothing = None

    @property
    def sequence_row_names(self) -> List[str]:
        return self._row_names

    @property
    def num_rows(self) -> int:
        return len(self._rows)

    @property
    def column_names(self) -> List[str]:
        return self._row_names

    def find_sequence(self, name: str) -> GenomeSequence:
        return self._named_sequences[name]


class Triangular(Matrix):
    pass


class Square(Matrix):
    pass
