class Parser:
    def __init__(self, rows) -> None:

        self._rows = list()
        for row in rows:
            self._rows.append(self._parse_row(row))

    def get_lines(self):
        return self._rows

    @staticmethod
    def _parse_row(row: str):
        split_row = row.split()
        parsed_row = list()
        parsed_row.append(split_row[0])
        for item in split_row[1:]:
            try:
                parsed_row.append(float(item))
            except ValueError:
                parsed_row.append(None)

        return parsed_row
