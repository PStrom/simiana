import itertools


def highest_match(e):
    return e[1] if e[1] else 0

class GenomeSequence:
    def __init__(self, sequence_name: str, comparison_names: list, comparison_values: list):

        for value in comparison_values:
            if value and not float(value):
                raise ValueError

        self._sequence_name = sequence_name
        self._comparison_values_by_names = dict(
            itertools.zip_longest(comparison_names, comparison_values, fillvalue=None)
        )
        self.values = list(
            itertools.zip_longest(comparison_names, comparison_values, fillvalue=None)
        )

        self.nothing = None

    def similarity_for(self, other_genome_sequence_name: str) -> str:
        return self._comparison_values_by_names.get(other_genome_sequence_name)

    @property
    def name(self) -> str:
        return self._sequence_name

    @property
    def ordered_values(self) -> list:
        return sorted(self.values, key=highest_match, reverse=True)

