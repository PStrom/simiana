from source import Matrix, GenomeSequence


class TestMatrix:
    def test_can_count_read_lines(self, fake_simple_triangle_lines, tri_matrix):
        assert tri_matrix.num_rows == len(fake_simple_triangle_lines)

    def test_column_gene_names(self, tri_matrix):
        assert tri_matrix.column_names == ['A', 'B', 'C', 'D']

    def test_row_gene_names(self, tri_matrix):
        assert tri_matrix.sequence_row_names == ['A', 'B', 'C', 'D']

    def test_row_data_parses_correctly_with_spaces_in_input(self, tri_matrix_spaces: Matrix):
        assert tri_matrix_spaces.sequence_row_names == ['A', 'B', 'C', 'D']
        assert tri_matrix_spaces.find_sequence('B').similarity_for('B') is None
        assert tri_matrix_spaces.find_sequence('D').similarity_for('A') == 74.55

    def test_matching_row_and_col_names(self, tri_matrix):
        assert tri_matrix.sequence_row_names == tri_matrix.column_names

    def test_matrix_can_find_gene_d(self, tri_matrix):
        gene_d = tri_matrix.find_sequence('D')
        assert isinstance(gene_d, GenomeSequence)
        assert gene_d.name == 'D'

    def test_matrix_can_find_gene_b(self, tri_matrix):
        gene_b = tri_matrix.find_sequence('B')
        assert isinstance(gene_b, GenomeSequence)
        assert gene_b.name == 'B'

    def test_matrix_gene_b_matches_a_correct(self, tri_matrix):
        sequence_b = tri_matrix.find_sequence('B')
        assert sequence_b.similarity_for('A') == 79.17

    def test_matrix_gene_d_matches_a_correct(self, tri_matrix):
        sequence_d = tri_matrix.find_sequence('D')
        assert sequence_d.similarity_for('A') == 74.55

    def test_matrix_gene_d_matches_b_correct(self, tri_matrix):
        sequence_d = tri_matrix.find_sequence('D')
        assert sequence_d.similarity_for('B') == 74.93
