class TestSquareInput:
    def test_can_read_the_square_matrix_file(self, square_file):
        num = sum(1 for l in square_file)
        assert num == 4

    def test_conftest_gives_correct_square_lines(self, square_lines):
        assert len(square_lines) == 4

    def test_raw_square_is_same_as_read(self, square_lines, fake_square_lines):
        assert len(square_lines) == len(fake_square_lines)


class TestTriangularInput:
    def test_can_read_the_triangle_matrix_file(self, triangle_file):
        num = sum(1 for l in triangle_file)
        assert num == 4

    def test_conftest_gives_correct_triangle_lines(self, triangle_lines):
        assert len(triangle_lines) == 4

    def test_raw_triangle_is_same_as_read(self, triangle_lines, fake_triangle_lines):
        assert len(triangle_lines) == len(fake_triangle_lines)
