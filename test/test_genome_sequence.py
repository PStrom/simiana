import pytest

from source import GenomeSequence


class TestGenomeSequence:
    def test_a_sequence_knows_its_name(self, sequence_d: GenomeSequence):
        assert sequence_d.name == 'D'

    def test_a_sequence_has_similarities(self, sequence_d: GenomeSequence):
        assert sequence_d._comparison_values_by_names == {'A': 74.55, 'B': 74.93, 'C': 75.27, 'D': None}

    def test_a_sequence_only_accepts_comparisons_with_value(self):
        with pytest.raises(ValueError):
            GenomeSequence('A', ['A', 'B', 'C'], [74.55, 73.92, 71.12, '---'])


    def test_a_sequence_shows_comparisons_with_values_only(self, sequence_d: GenomeSequence):
        pass