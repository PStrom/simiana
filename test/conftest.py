import pytest

from source import Matrix, GenomeSequence, Writer, Parser


@pytest.fixture(name='square_file')
def square_file_object():
    with open('../files/SquareMatrix.txt') as file:
        yield file


@pytest.fixture
def square_lines(square_file):
    return [l for l in square_file]


@pytest.fixture
def fake_square_lines():
    return [
        "s_gordonii_CCUG_33482T.fna	---	78.95	79.24	74.53",
        "s_oligofermentans_AS_1.3089T_RefSeq.fasta	79.17	---	92.68	75.06",
        "s_cristatus_ATCC_51100T_RefSeq_5.fna	79.51	92.55	---	74.99",
        "SoralisDentisani7747TCAUK01.1.fsa_nt	74.55	74.93	75.27	---"
    ]


@pytest.fixture(name='triangle_file')
def triangle_file_object():
    with open('../files/LowTriangularMatrix.txt') as file:
        yield file


@pytest.fixture
def fake_simple_square_lines():
    return [
        "A  ---	78.95	79.24	74.53",
        "B  79.17	---	92.68	75.06",
        "C  79.51	92.55	---	74.99",
        "D  74.55	74.93	75.27	---",
    ]


@pytest.fixture
def fake_simple_square_lines_with_spaces():
    return [
        "A  ---  78.95  79.24  74.53",
        "B  79.17  ---  92.68  75.06",
        "C  79.51  92.55  ---  74.99",
        "D  74.55  74.93  75.27  ---",
    ]


@pytest.fixture
def triangle_lines(triangle_file):
    return [l for l in triangle_file]


@pytest.fixture
def fake_triangle_lines():
    return [
        "s_gordonii_CCUG_33482T.fna	---",
        "s_oligofermentans_AS_1.3089T_RefSeq.fasta	79.17	---",
        "s_cristatus_ATCC_51100T_RefSeq_5.fna	79.51	92.55	---",
        "SoralisDentisani7747TCAUK01.1.fsa_nt	74.55	74.93	75.27	---",
    ]


@pytest.fixture(name='fake_simple_triangle_lines')
def _fake_simple_triangle_lines():
    return [
        "A  ---",
        "B	79.17  ---",
        "C	79.51  92.55	---",
        "D	74.55  74.93	75.27	---",
    ]


@pytest.fixture(name='fake_simple_triangle_lines_with_spaces')
def _fake_simple_triangle_lines_with_spaces():
    return [
        "A  ---",
        "B  79.17  ---",
        "C  79.51  92.55  ---",
        "D  74.55  74.93  75.27  ---",
    ]


@pytest.fixture
def sequence_d():
    return GenomeSequence('D',
                          ['A', 'B', 'C', 'D'],
                          [74.55, 74.93, 75.27, None]
                          )


@pytest.fixture
def tri_matrix(fake_simple_triangle_lines):
    parser = Parser(fake_simple_triangle_lines)
    return Matrix(parser.get_lines())


@pytest.fixture
def tri_matrix_spaces(fake_simple_triangle_lines_with_spaces):
    parser = Parser(fake_simple_triangle_lines_with_spaces)
    return Matrix(parser.get_lines())


@pytest.fixture
def a_parser():
    return Parser('../files/SquareMatrix.txt')


@pytest.fixture
def a_writer():
    return Writer()
