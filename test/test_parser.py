from source import Parser


class TestParser:
    def test_parser_converts_non_numeric_values_to_none(self, fake_simple_triangle_lines):
        parser = Parser(fake_simple_triangle_lines)

        assert parser.get_lines()[0] == ['A',None]
