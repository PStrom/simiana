from source.genomesequence import GenomeSequence


class TestWriter:
    def test_not_sorting_maintains_order(self, sequence_d):
        assert sequence_d.values == [('A', 74.55),
                                     ('B', 74.93),
                                     ('C', 75.27),
                                     ('D', None)]

    def test_sorting_puts_genome_sequences_in_order(self, sequence_d: GenomeSequence):
        assert sequence_d.ordered_values == [('C', 75.27),
                                             ('B', 74.93),
                                             ('A', 74.55),
                                             ('D', None)]
