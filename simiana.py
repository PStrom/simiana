#!/usr/bin/env python3

import argparse
import os
import sys

from config import settings
from source import Matrix, Parser, PrintWriter, FileWriter


class Main:
    @staticmethod
    def run():
        arg_parser = argparse.ArgumentParser(
            formatter_class=argparse.RawTextHelpFormatter,
            description="""
            Display a similarity matrix in a line by line comparison way.
            Example usage: 
            ./simiana.py files/simple_square_1.txt --output=print -o n
                        """)
        arg_parser.add_argument(
            'file',
            type=str,
            help='The file to run the similarity comparison on.'
        )
        arg_parser.add_argument(
            '--output',
            type=str,
            default='file',
            help='The type of output to use, file or print, defaults to file'
        )
        arg_parser.add_argument(
            '-o',
            '--order',
            type=str,
            default='y',
            help='Whether the genome sequence comparisons should be ordered or not. (y/n)'
        )

        if len(sys.argv) == 1:
            arg_parser.print_help()
            sys.exit(1)

        args = arg_parser.parse_args()
        with open(args.file, 'r') as file:
            lines_with_data = [l for l in file]

        parser = Parser(lines_with_data)
        matrix = Matrix(parser.get_lines())

        read_path, read_file_name = os.path.split(args.file)
        output_file_name = "{}{}".format(settings.OUTPUT_PREFIX, read_file_name)
        output_path = os.path.join(read_path, output_file_name)

        do_ordering = args.order.strip().lower()
        try:
            if args.output == 'file':
                formatter = FileWriter(settings.OUTPUT_SEPARATOR, output_name=output_path, do_ordering=do_ordering)
            else:
                formatter = PrintWriter(settings.OUTPUT_SEPARATOR, do_ordering=args.order)

            formatter.output(matrix)
            print('New file generated: {}'.format(output_path))
        except IOError as m:
            print(m)


if __name__ == "__main__":
    Main.run()
