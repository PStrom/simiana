# Simiana #

A tool to help make gene similarity comparisons easier.


## Prerequisites ##
`python3.5` or higher to be installed, and on the path.

## Installation ##

No installation needed, you can just download the files.
I recommend using Git though, as it facilitates easy updates.
Just clone this where you would like the program to reside.

    
    git clone https://bitbucket.org/PStrom/simiana.git .
    
_(when in the directory where you would like it to be downloaded)_

## Update ##

If you cloned this using git, then just typing ```git pull``` in the root directory of the script will update it.

## How to use ##
Run the code, by typing the following code in a terminal:

```
python3 simiana.py filename
```

Add -h for help on the command line.

Note, the first line of the file should contain the data, not any column headers.  
Simiana is built with the assumption that the matrix is correct, 
and that the gene sequences occur in the same order on both the x and y axis.


## Author ##
Peder Strömberg <pederstromberg@gmail.com>

## Licence ##
MIT

Copyright 2017 Peder Strömberg

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
